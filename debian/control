Source: python-pex
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Barry Warsaw <barry@debian.org>
Homepage: https://github.com/pantsbuild/pex
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-python,
               help2man,
               python3-sphinx,
               python3-all,
               python3-setuptools,
               python3-wheel
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/python-team/modules/python-pex.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/python-pex

Package: python3-pex
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Suggests: python-pex-doc
Description: library for generating Python executable zip files
 pex is a library for generating .pex (Python EXecutable) files which
 are executable Python environments in the spirit of virtualenvs.  pex
 is an expansion upon the ideas outlined in PEP 441 and makes the
 deployment of Python applications as simple as cp.  pex files may even
 include multiple platform-specific Python distributions, meaning that
 a single pex file can be portable across Linux and OS X.
 .
 pex files can be built using the pex tool.  Build systems such as
 Pants and Buck also support building .pex files directly.
 .
 This is the Python 3 library version.

Package: pex
Architecture: all
Depends: python3,
         python3-pex,
         python3-pkg-resources,
         python3-requests,
         python3-setuptools,
         python3-wheel,
         ${misc:Depends},
         ${python3:Depends}
Replaces: python-pex-cli (<< 1.0.3-2~)
Breaks: python-pex-cli (<< 1.0.3-2~)
Suggests: python-pex-doc
Description: library for generating Python executable zip files
 pex is a library for generating .pex (Python EXecutable) files which
 are executable Python environments in the spirit of virtualenvs.  pex
 is an expansion upon the ideas outlined in PEP 441 and makes the
 deployment of Python applications as simple as cp.  pex files may even
 include multiple platform-specific Python distributions, meaning that
 a single pex file can be portable across Linux and OS X.
 .
 pex files can be built using the pex tool.  Build systems such as
 Pants and Buck also support building .pex files directly.
 .
 This is the command line package.

Package: python-pex-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: library for generating Python executable zip files
 pex is a library for generating .pex (Python EXecutable) files which
 are executable Python environments in the spirit of virtualenvs.  pex
 is an expansion upon the ideas outlined in PEP 441 and makes the
 deployment of Python applications as simple as cp.  pex files may even
 include multiple platform-specific Python distributions, meaning that
 a single pex file can be portable across Linux and OS X.
 .
 pex files can be built using the pex tool.  Build systems such as
 Pants and Buck also support building .pex files directly.
 .
 This is the common documentation package.
